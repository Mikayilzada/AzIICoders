<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

     public function __construct(){
        parent::__construct();
        $this->load->model('UserModel');
      }

    public function index()
    {
        $this->load->view('user/login');
    }

    public function register()
    {
        $this->load->view('user/register');
    }

    public function regAct(){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $phone    = $_POST['phone'];
        $gender   = $_POST['gender'];

        if($this->UserModel->check_email($email) != TRUE){


                    if(!empty($name) && !empty($email) && !empty($password) && !empty($phone) && !empty($gender)  ){
                            $user_array = array(
                                'name' => $name,
                                'email' => $email,
                                'password' => md5($password),
                                'gender' => $gender,
                                'number' => $phone,
                                'status' => '0'
                            );

                            $this->UserModel->insert($user_array);


                        $this->session->set_userdata('register_success','<span style="color:lightgreen">You have registered successfully</span>');
                        redirect(base_url('UserController'));

                    }else{
                        $this->session->set_userdata('register_error','<span style="color:pink">Please fill all blanks for register</span>');
                        redirect(base_url('UserController/register'));
                    }


        }else{
                $this->session->set_userdata('register_error_email','<span style="color:pink">Email already taken</span>');
                redirect(base_url('UserController/register'));
        }
    }



    public function loginAct(){
        $email = $_POST['email'];
        $password = $_POST['password'];

        if(!empty($email) && !empty($password)){

            
            if($this->UserModel->loginCheck($email,$password) == TRUE){

                    $_SESSION['user_login'] = TRUE;
                    $_SESSION['user_data'] =  $this->UserModel->loginCheck($email,$password);

                    redirect(base_url('UserController/admin'));

            }else{
                $this->session->set_userdata('login_error_email_password','<span style="color:pink">Email or password failed</span>');
                redirect(base_url('UserController'));
            }

        }else{
            $this->session->set_userdata('login_error_empty','<span style="color:pink">Please fill all blanks for login</span>');
            redirect(base_url('UserController'));
        }
    }


    public function insert()
    {
        $data['all_shops'] = $this->db->get('shop')->result_array();
        $data['all_problems'] = $this->db->get('problem')->result_array();
        $this->load->view("user/userAdd",$data);
    }

    public function insertAct()
    {
        $devices_name = strip_tags($_POST['devices_name']);
        $devices_desc = strip_tags($_POST['devices_desc']);
        $device_shop  = strip_tags($_POST['device_shop']);
        $device_problem = $_POST['device_problem'];



        if(!empty($devices_name) && !empty($devices_desc) && !empty($device_shop)){
            $insertData = array(
                'devices_name' => $devices_name,
                'devices_desc' => $devices_desc,
                'devices_shop_id' => $device_shop,
                'devices_user_id' => $_SESSION['user_data'][0]['id'],
                'devices_completeleness' => 0,
                'devices_status' => 0
            );


            $insert_id = $this->UserModel->insertAdd($insertData);

            for($d=0;$d<count($device_problem);$d++){
                $device_problem_data = array(
                    'devices_id' => $insert_id,
                    'devices_problem_id' => $device_problem[$d]
                );

                $this->db->insert('devices_problem',$device_problem_data);
            }

            $all_user_problem = $this->db->join('problem','problem.problem_id = devices_problem.devices_problem_id')->where('devices_id',$insert_id)->get('devices_problem')->result_array();
            $new_array = array();
            foreach($all_user_problem as $all_user_problem_key){

                array_push($new_array, $all_user_problem_key['price']);
            }
            $full_price = array_sum($new_array);
            
            $update_device_price = array(
                'devices_price' => $full_price
            );

            $this->db->where('devices_id',$insert_id)->update('devices',$update_device_price);





            redirect(base_url("UserController/admin"));
        }else{
            $this->session->set_userdata('add_empty_error','<span style="color:red;font-size:20px">Please fill all blanks for add device repair</span>');
            redirect(base_url('UserController/insert'));
        }


    }

    public function admin()
    {
        $data['user_devices'] = $this->db->where('devices_user_id',$_SESSION['user_data'][0]['id'])->get('devices')->result_array();
        $this->load->view('user/main',$data);
    }

    public function logOut(){

        if(isset($_SESSION['user_login']))
                unset($_SESSION['user_login']);
              redirect(base_url('UserController'));

    }








}
