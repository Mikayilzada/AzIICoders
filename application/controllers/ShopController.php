<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShopController extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('ShopModel');
    }


    public function index()
    {
        $this->load->view('shop/login');
    }

    public function logAct(){
    	$email = $_POST['email'];
    	$password = $_POST['password'];

    	if(!empty($email) && !empty($password)){


    		if($this->ShopModel->checkSHop($email,$password) == TRUE){
    			

    			    $_SESSION['shop_login'] = TRUE;
                    $_SESSION['shop_data'] =  $this->ShopModel->checkSHop($email,$password);

                    redirect(base_url('ShopController/admin'));


    		}else{

    			$this->session->set_userdata('shop_login_error','<span style="color:pink">Email or password failed</span>');
    			redirect(base_url('ShopController'));

    		}

    	}else{
    		$this->session->set_userdata('shop_login_error','<span style="color:pink>Email or password failed</span>');
    		redirect(base_url('ShopController'));
    	}

    }   



    public function logOut(){

        if(isset($_SESSION['shop_login']))
                unset($_SESSION['shop_login']);
              redirect(base_url('ShopController'));

    }

    public function admin()
    {
        $this->load->view('shop/main');
    }


    public function device(){
        $data['all_services']     = $this->db->where('service_shop_id',$_SESSION['shop_data'][0]['shop_id'])->get('service')->result_array();
        $data['all_shop_devices'] = $this->db->order_by('devices_id','DESC')->where('devices_shop_id',$_SESSION['shop_data'][0]['shop_id'])->join('service','service.service_id=devices.devices_service_id','left')->join('user','user.id = devices.devices_user_id')->get('devices')->result_array();


        $data['all_shop_devices_data'] = $this->db->where('shop_id',$_SESSION['shop_data'][0]['shop_id'])->join('service','service.service_id = shop_service.service_id')->get('shop_service')->result_array();


    	$this->load->view('shop/device',$data);
    }

    public function addDeviceService($id,$user_id){

        $shop=$_POST['shop'];
        $data = array(
            'devices_service_id' => $shop
        );

        $user_data = $this->db->where('id',$user_id)->get('user')->result_array();
        $user_email = $user_data[0]['email'];

                        $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'smtp.gmail.com',
                            'smtp_port' => 587,
                            'smtp_user' => 'bumeranq.info@gmail.com',
                            'smtp_pass' => 'alfred1952',
                            'mailtype'  => 'html',
                            'charset'   => 'iso-8859-1'
                        );
                        $this->load->library('email', $config);
                        $this->email->from('bumeranq.info@gmail.com');
                        $this->email->to($user_email);
                        $this->email->subject('AziiCoders');
                        $this->email->message('Azii Coders');
                        $this->email->send();


        $this->ShopModel->addDeviceService($id,$data);
        redirect(base_url('ShopController/device'));


    }

    public function services(){
        $this->load->view('shop/service');

    }




}
