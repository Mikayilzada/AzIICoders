<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuperAdminController extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('SuperAdminModel');
    }

    public function index()
    {
        $this->load->view('super_admin/login');
    }

    public function loginAct(){
    	$email = $_POST['email'];
    	$password = $_POST['password'];

    	if(!empty($email) && !empty($password)){
    		if($this->SuperAdminModel->loginCheck($email,$password) == TRUE){

    			$_SESSION['super_admin_login'] = TRUE;
    			$_SESSION['super_admin_data'] = $this->SuperAdminModel->loginCheck($email,$password);

    			redirect(base_url('SuperAdminController/admin'));

    		}else{
    			$this->session->set_userdata('error_super_admin_login','<span style="color:pink">Email or Password failed</span>');
    			redirect(base_url('SuperAdminController'));
    		}
    	}else{
    			$this->session->set_userdata('error_super_admin_empty','<span style="color:pink">Please fill all blanks for login</span>');
    			redirect(base_url('SuperAdminController'));
    	}

    }


    public function admin()
    {
        $this->load->view('super_admin/main');
    }

    public function logOut(){

        if(isset($_SESSION['super_admin_login']))
            unset($_SESSION['super_admin_login']);
            redirect(base_url('SuperAdminController'));

    }

    public function services(){
    	$data['all_shop']     = $this->SuperAdminModel->getAllShops();
    	$data['all_services'] = $this->SuperAdminModel->getAllServices();
    	$data['all_shop_and_services'] = $this->SuperAdminModel->getAllShopAndService();
    	$this->load->view('super_admin/service',$data);
    }

    public function addService(){
    	$service_name = strip_tags($_POST['service_name']);
    	$service_phone = strip_tags($_POST['service_phone']);
    	$service_capacity = strip_tags($_POST['service_capacity']);
        $service_email    = strip_tags($_POST['service_email']);
        $service_password = strip_tags($_POST['service_password']);
    	if(!empty($service_name) && !empty($service_phone) && !empty($service_capacity)){
	    	$service_data = array(

	    		'service_name' => $service_name,
	    		'service_phone' => $service_phone,
	    		'service_capacity' => $service_capacity,
                'service_email'    => $service_email,
                'service_password' => $service_password

	    	);

	    	$this->SuperAdminModel->addServiceModel($service_data);
	    	$this->session->set_userdata('success_add_service','<span style="color:green">New service added successfully</span>');
	    	redirect(base_url('SuperAdminController/services'));
    	}else{
    		$this->session->set_userdata('error_add_service','<b style="color:red">Be sure that you filled all blanks</b>');
	    	redirect(base_url('SuperAdminController/services'));
    	}

    }

    public function addShopService(){
    	$shop = $_POST['shop'];
    	$service = $_POST['service'];

    	if(!empty($shop) && !empty($service)){

    		if($this->SuperAdminModel->checkShopService($shop,$service) != TRUE){
		    	$shopServiceData = array(
		    		'shop_id' => $shop,
		    		'service_id' => $service
		    	);

		    	$this->db->insert('shop_service',$shopServiceData);
		    	$this->session->set_userdata('success_add_shop_service','<b style="color:green">Add Shop to Service Successfully</b>');
		    	redirect(base_url('SuperAdminController/services'));
    		}else{
		    	$this->session->set_userdata('error_add_shop_service_taken','<b style="color:red">This connection allready created</b>');
		    	redirect(base_url('SuperAdminController/services'));
    		}





    	}else{
    		$this->session->set_userdata('error_empty_shop_service','<b style="color:red">Be sure that you filled all blanks</b>');
    		redirect(base_url('SuperAdminController/services'));
    	}



    }


    public function problem(){
        $data['all_problems'] = $this->db->get('problem')->result_array();
        $this->load->view('super_admin/problem',$data);
    }

    public function addProblem(){
        $problem_name = $_POST['problem_name'];
        $problem_price = $_POST['problem_price'];

        if(!empty($problem_name) && !empty($problem_price)){
            $problem_data = array(
                'problem_name' => $problem_name,
                'price' => $problem_price
            );

            $this->SuperAdminModel->addNewProblem($problem_data);
            $this->session->set_userdata('success_add_problem','<b style="color:green">Problem add successfully</b>');
            redirect(base_url('SuperAdminController/problem'));


        }else{
            $this->session->set_userdata('error_empty_problem','<b style="color:red">Please fill all fields</b>');
            redirect(base_url('SuperAdminController/problem'));
        }
    }


    public function updateProblem($id){

        $problem_name = $_POST['name'];
        $problem_price = $_POST['price'];

        if(!empty($problem_name) && !empty($problem_price)){
            $problem_data = array(
                'problem_name' => $problem_name,
                'price' => $problem_price
            );

            $this->SuperAdminModel->updateProblem($id,$problem_data);
            redirect(base_url('SuperAdminController/problem'));


        }else{
            redirect(base_url('SuperAdminController/problem'));
        }

    }

    public function deleteProblem($id){
        $this->db->where('problem_id',$id)->delete('problem');
        redirect(base_url('SuperAdminController/problem'));
    }

    public function updateService($id){

        $service_name = strip_tags($_POST['name']);
        $service_phone = strip_tags($_POST['phone']);
        $service_capacity = strip_tags($_POST['capacity']);
        $service_email    = strip_tags($_POST['email']);
        $service_password = strip_tags($_POST['password']);
        if(!empty($service_name) && !empty($service_phone) && !empty($service_capacity)){

            $service_data = array(

                'service_name' => $service_name,
                'service_phone' => $service_phone,
                'service_capacity' => $service_capacity,
                'service_email'    => $service_email,
                'service_password' => $service_password

            );

            $this->SuperAdminModel->updateServiceModel($id,$service_data);
            redirect(base_url('SuperAdminController/services'));
        }else{
            redirect(base_url('SuperAdminController/services'));
        }

    }

    public function deleteService($id){
        $this->db->where('service_id',$id)->delete('service');
         redirect(base_url('SuperAdminController/services'));
    }

    public function deleteServiceShop($id){
        $this->db->where('id',$id)->delete('shop_service');
        redirect(base_url('SuperAdminController/services'));
    }


    public function shop(){
        $data['all_shop_details'] = $this->SuperAdminModel->allShopData();
        $this->load->view('super_admin/shop',$data);
    }
    public function addShopAct(){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $phone = $_POST['phone'];
        $lng = $_POST['lng'];
        $lat = $_POST['lat'];

        if(!empty($name) && !empty($email) && !empty($password) && !empty($phone)){

            $shop_data = array(
                'shop_name' => $name,
                'shop_email' => $email,
                'shop_password' => $password,
                'shop_phone'=>$phone,
                'shop_lng'=>$lng,
                'shop_lat'=>$lat,
            );

            $this->SuperAdminModel->insertShopData($shop_data);
            $this->session->set_userdata('success_shop_add','<b style="color:green">Shop add successfully</b>');
            redirect(base_url('SuperAdminController/shop'));

        }else{
            $this->session->set_userdata('error_shop_add_empty','<b style="color:red">Please, fill all fields</b>');
            redirect(base_url('SuperAdminController/shop'));
        }

    }

    public function updateShopAct($id){

        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $phone = $_POST['phone'];
        $lng = $_POST['lng'];
        $lat = $_POST['lat'];

        if(!empty($name) && !empty($email) && !empty($phone)){

            $shop_data = array(
                'shop_name' => $name,
                'shop_email' => $email,
                'shop_password' => $password,
                'shop_phone'=>$phone,
                'shop_lng'=>$lng,
                'shop_lat'=>$lat,
            );

            $this->SuperAdminModel->updateShopData($id,$shop_data);
            redirect(base_url('SuperAdminController/shop'));

        }else{
            redirect(base_url('SuperAdminController/shop'));
        }

    }

    public function deleteShop($id){
        $this->db->where('shop_id',$id)->delete('shop');
        redirect(base_url('SuperAdminController/shop'));
    }


}
