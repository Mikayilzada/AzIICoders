<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServiceController extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('ServiceModel');
    }

    public function index()
    {
        $this->load->view('service/login');
    }

    public function admin()
    {   
        $this->load->view('service/main');
    }

    public function loginAct(){
    	$email = $_POST['email'];
    	$password = $_POST['password'];

        if(!empty($email) && !empty($password)){
            if($this->ServiceModel->loginCheck($email,$password) == TRUE){
                $_SESSION['service_login'] = TRUE;
                $_SESSION['service_data']  = $this->ServiceModel->loginCheck($email,$password);

                redirect(base_url('ServiceController/admin'));

            }else{
            $this->session->set_userdata('service_login_error','<span style="color:pink">Email or Password failed</span>');
            redirect(base_url('ServiceController'));
            }
        }else{
           $this->session->set_userdata('service_login_empty_error','<span style="color:pink">Please, fill all fields</span>');
            redirect(base_url('ServiceController'));
        }
    	

    }

    public function logOut(){

        if(isset($_SESSION['service_login']))
                unset($_SESSION['service_login']);
              redirect(base_url('ServiceController'));
    }

    public function device(){
        $data['all_service_devices'] = $this->db->join('user','user.id = devices.devices_user_id')->where('devices_service_id',$_SESSION['service_data'][0]['service_id'])->get('devices')->result_array();
        $this->load->view('service/device',$data);
    }

    public function addStartAndEndDate($id){

        $start_date = $_POST['start_date'];
        $end_date   = $_POST['end_date'];

        if(!empty($start_date) && !empty($end_date)){


            $date_data = array(
                'devices_start_date' => $start_date,
                'devices_end_date'   => $end_date
            );

            $this->ServiceModel->addDeviceRepairDate($id,$date_data);
            $this->session->set_userdata('add_start_end_date_success','Starting ');
            redirect(base_url('ServiceController/device'));

        }else{
            redirect(base_url('ServiceController/device'));
        }

    }


    public function addDeviceStatus($id){

        $device_status = $_POST['device_status'];

        $device_status_data = array(
            'devices_status' => $device_status
        );

        $this->ServiceModel->addDeviceStatus($id,$device_status_data);
        redirect(base_url('ServiceController/device'));


    }

}
