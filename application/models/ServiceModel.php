<?php

class ServiceModel extends CI_Model
{

	public function loginCheck($email,$password){

		return $this->db->where('service_password',$password)->where('service_email',$email)->get('service')->result_array();

	}	

	public function addDeviceRepairDate($id,$date_data){

		$this->db->where('devices_id',$id)->update('devices',$date_data);

	}

	public function addDeviceStatus($id,$status_data){
		$this->db->where('devices_id',$id)->update('devices',$status_data);
	}

}

?>