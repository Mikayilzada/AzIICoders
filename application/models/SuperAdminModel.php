<?php

class SuperAdminModel extends CI_Model
{
	
	public function loginCheck($email,$password){
		return $this->db->where('status',4)->where('email',$email)->where('password',md5($password))->get('user')->result_array();
	}

	public function addServiceModel($service_data){
		$this->db->insert('service',$service_data);
	}

	public function updateServiceModel($id,$service_data){
		$this->db->where('service_id',$id)->update('service',$service_data);
	}

	public function getAllServices(){
		return $this->db->get('service')->result_array();
	}

	public function getAllShops(){
		return $this->db->get('shop')->result_array();
	}

	public function checkShopService($shop_id,$service_id){
		return $this->db->where('shop_id',$shop_id)->where('service_id',$service_id)->get('shop_service')->result_array();
	}

	public function getAllShopAndService(){
		return $this->db->join('shop','shop.shop_id = shop_service.shop_id')->join('service','service.service_id = shop_service.service_id')->get('shop_service')->result_array();
	}

	public function addNewProblem($problem_data){
		$this->db->insert('problem',$problem_data);
	}

	public function updateProblem($id,$problem_data){
		$this->db->where('problem_id',$id)->update('problem',$problem_data);
	}


	public function insertShopData($shop_data){
		$this->db->insert('shop',$shop_data);
	}

	public function allShopData(){
		return $this->db->get('shop')->result_array();
	}

	public function updateShopData($id,$shop_data){
		$this->db->where('shop_id',$id)->update('shop',$shop_data);
	}


}

?>