<?php

if(isset($_SESSION['user_login']) == FALSE){
    redirect(base_url('UserController'));
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url('public/css/style.css') ?> ">
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>



<body>
<?php print_r($_SESSION['user_data']) ?>
<div class="header">
    <a href="#" id="menu-action">
        <i class="fa fa-bars"></i>
        <span>Close</span>
    </a>
    <div class="logo">
        User Admin
    </div>
</div>
<div class="sidebar">
    <ul>
        <li><a href="<?php echo base_url("UserController/admin") ?>"><i class="fa fa-desktop"></i><span>Main</span></a></li>
        <li><a href="<?php echo base_url("UserController/insert") ?>"><i class="fa fa-server"></i><span>Add</span></a></li>
        <li><a href="<?php echo base_url('UserController/logOut'); ?>"><i class="fa fa-sign-out"></i><span>Log out</span></a></li>
    </ul>
</div>

<!-- Content -->

<div class="main">
    <div class="hipsum">

        <div class="jumbotron">

            <?php

                if(empty($user_devices)){ ?>
                        <h4>You dont have any device for repair</h4>
                    <?php }else{ ?>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Device name</th>
                  <th scope="col">Description</th>
                  <th scope="col">Repair time</th>
                  <th scope="col">Price</th>
                  <th scope="col">Status</th>
                </tr>
              </thead>
              <tbody>


                <?php foreach($user_devices as $user_devices_key){ ?>

                    <tr>
                      <th scope="row"><?php echo $user_devices_key['devices_id'] ?></th>
                      <td><?php echo $user_devices_key['devices_name'] ?></td>
                      <td><?php echo $user_devices_key['devices_desc'] ?></td>
                      <td><?php echo $user_devices_key['devices_start_date'].' - '.$user_devices_key['devices_end_date'] ?></td>
                      <td><?php echo $user_devices_key['devices_price'] ?> AZN</td>
                      <td>
                        <?php if($user_devices_key['devices_status'] == 0){ ?>
                                <span style="color: orange"><b>Pending</b></span>
                            <?php }elseif($user_devices_key['devices_status'] == 1){ ?>
                                <span style="color: blue"><b>Repair started</b></span>
                            <?php }elseif($user_devices_key['devices_status'] == 2){ ?>
                                <span style="color: green"><b>Finished  </b></span>
                            <?php }else{ ?>
                                <span style="color: red"><b>Aborted</b></span>
                            <?php } ?>      
                      </td>
                    </tr>

                <?php } ?>





              </tbody>
            </table>
                        <?php }

            ?>



    
        </div>


        <div class="row">
<!--            <div class="col-sm-6">-->
<!--                <p>Slow-carb fanny pack yr Brooklyn gentrify. Fanny pack keffiyeh taxidermy, ugh viral polaroid craft beer. +1 distillery Truffaut typewriter tousled crucifix, lo-fi butcher normcore skateboard. Drinking vinegar ugh whatever sriracha. Synth tofu-->
<!--                    viral butcher flexitarian. 3 wolf moon Schlitz plaid small batch kale chips blog. Fingerstache selfies freegan, Helvetica Neutra Brooklyn semiotics cred narwhal beard tousled leggings.</p>-->
<!--            </div>-->
<!--            <div class="col-sm-6">-->
<!--                <p>Slow-carb fanny pack yr Brooklyn gentrify. Fanny pack keffiyeh taxidermy, ugh viral polaroid craft beer. +1 distillery Truffaut typewriter tousled crucifix, lo-fi butcher normcore skateboard. Drinking vinegar ugh whatever sriracha. Synth tofu-->
<!--                    viral butcher flexitarian. 3 wolf moon Schlitz plaid small batch kale chips blog. Fingerstache selfies freegan, Helvetica Neutra Brooklyn semiotics cred narwhal beard tousled leggings.</p>-->
<!--            </div>-->
        </div>
    </div>
</div>
</body>



</html>
<script src="<?php echo base_url("public/js/main.js") ?>"></script>

