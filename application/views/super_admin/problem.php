<?php

if(isset($_SESSION['super_admin_login']) == FALSE){
    redirect(base_url('SuperAdminController'));
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Super Admin Problem Add</title>
    <link rel="stylesheet" href="<?php echo base_url('public/css/style.css') ?> ">
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>


<body>

<div class="header">
    <a href="#" id="menu-action">
        <i class="fa fa-bars"></i>
        <span>Close</span>
    </a>
    <div class="logo">
       Super Admin
    </div>
</div>
<div class="sidebar">
    <ul>
        <li><a href="<?php echo base_url('SuperAdminController/admin') ?>"><i class="fa fa-home"></i><span>Main</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/problem') ?>"><i class="fa fa-exclamation-triangle"></i><span>Problem</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/shop') ?>"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span>Shop</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/services') ?>"><i class="fa fa-server"></i><span>Services</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/logOut'); ?>"><i class="fa fa-sign-out"></i>Log out</a></li>
    </ul>
</div>

<!-- Content -->
<div class="main">
    <div class="hipsum">
                <div class="jumbotron">
                    <h4>Add Problem</h4>
                    <?php echo $this->session->userdata('success_add_problem'); $this->session->unset_userdata('success_add_problem'); ?>
                    <?php echo $this->session->userdata('error_empty_problem'); $this->session->unset_userdata('error_empty_problem'); ?>
                    <form action="<?php echo base_url('SuperAdminController/addProblem') ?>" method="POST">
                        <div class="form-group">
                            <input type="text" name="problem_name" placeholder="Problem name" class="form-control">
                            <br>
                            <input type="number" name="problem_price" placeholder="Add problem price" class="form-control">
                            <br>
                            <input type="submit" value="Add" class="btn btn-primary">
                        </div>
                    </form>
                </div>
       
                <div class="jumbotron">
                    <table class="table table-hover">
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Problem Name</th>
                          <th scope="col">Price</th>
                          <th scope="col">Operation</th>
                        </tr>
                      </thead>
                      <tbody>


                        <?php for($f=0;$f<count($all_problems);$f++){ ?>
                            <tr>
                              <th><?php echo $all_problems[$f]['problem_id'] ?></th>
                              <td><?php echo $all_problems[$f]['problem_name'] ?></td>
                              <td><?php echo $all_problems[$f]['price'] ?> AZN</td>
                              <td>

                                  <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#mySecondModal<?php echo $f ?>">update</button>

                                 

                                  <a href="<?php echo base_url('SuperAdminController/deleteProblem/'.$all_problems[$f]['problem_id']); ?>" class="btn btn-danger btn-xs">delete</a>




                                    <div id="mySecondModal<?php echo $f ?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">modal for update</h4>
                                          </div>
                                          <div class="modal-body">
                                           <form action="<?php echo base_url('SuperAdminController/updateProblem/'.$all_problems[$f]['problem_id']) ?>" method="POST">
                                            <label for="">Problem name</label>
                                                <input type="text" value="<?php echo $all_problems[$f]['problem_name'] ?>" class="form-control" name="name" placeholder="Problem name">
                                                <br>
                                                <label for="">Price</label>
                                                <input type="text" value="<?php echo $all_problems[$f]['price'] ?>" class="form-control" name="price" placeholder="Price">
                                                <br>
                                                <input type="submit" class="btn btn-primary" value="Change">
                                            </form>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>

                              </td>
                            </tr>
                        <?php } ?>


                      </tbody>
                    </table>
                </div>
    </div>
</div>
</body>



</html>
<script src="<?php echo base_url("public/js/main.js") ?>"></script>

