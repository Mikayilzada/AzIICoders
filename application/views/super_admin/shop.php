<?php

if(isset($_SESSION['super_admin_login']) == FALSE){
    redirect(base_url('SuperAdminController'));
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Super Admin Shop</title>
    <link rel="stylesheet" href="<?php echo base_url('public/css/style.css') ?> ">
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>


<body>

<div class="header">
    <a href="#" id="menu-action">
        <i class="fa fa-bars"></i>
        <span>Close</span>
    </a>
    <div class="logo">
       Super Admin
    </div>
</div>
<div class="sidebar">
    <ul>
        <li><a href="<?php echo base_url('SuperAdminController/admin') ?>"><i class="fa fa-home"></i><span>Main</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/problem') ?>"><i class="fa fa-exclamation-triangle"></i><span>Problem</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/shop') ?>"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span>Shop</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/services') ?>"><i class="fa fa-server"></i><span>Services</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/logOut'); ?>"><i class="fa fa-sign-out"></i>Log out</a></li>
    </ul>
</div>

<!-- Content -->
<div class="main">
    <div class="hipsum">
        <div class="jumbotron">
            <?php echo $this->session->userdata('success_shop_add'); $this->session->unset_userdata('success_shop_add'); ?>
            <?php echo $this->session->userdata('error_shop_add_empty'); $this->session->unset_userdata('error_shop_add_empty'); ?>
    <br>

            <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">Add Shop</button>

            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo base_url('SuperAdminController/addShopAct') ?>"  method="POST">
                        <input name="name" type="text" class="form-control" placeholder="Name"><br>

                        <input name="email" type="text" class="form-control" placeholder="Email"><br>

                        <input name="password" type="text" class="form-control" placeholder="Password"><br>

                        <input name="phone" type="text" class="form-control" placeholder="Phone"><br>

                        <input name="lat" type="text" class="form-control" placeholder="Map lat"><br>

                        <input name="lng" type="text" class="form-control" placeholder="Map lng"><br>
                        <input type="submit" value="Add" class="btn btn-primary">
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>






        </div>

        <div class="jumbotron">

            
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Name</th>
                      <th scope="col">Email</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Map lat</th>
                      <th scope="col">Map lng</th>
                      <th scope="col">Operations</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php for($m=0;$m<count($all_shop_details);$m++){ ?> 
                        <tr>
                          <th scope="row"><?php echo $m+1 ?></th>
                          <td><?php echo $all_shop_details[$m]['shop_name'] ?></td>
                          <td><?php echo $all_shop_details[$m]['shop_email'] ?></td>
                          <td><?php echo $all_shop_details[$m]['shop_phone'] ?></td>
                          <td><?php echo $all_shop_details[$m]['shop_lat'] ?></td>
                          <td><?php echo $all_shop_details[$m]['shop_lng'] ?></td>
                          <td>
                              <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal<?php echo $m ?>">Update</button>
                              <a href="<?php echo base_url('SuperAdminController/deleteShop/'.$all_shop_details[$m]['shop_id']) ?>" class="btn btn-danger btn-xs">delete</a>


                              <div id="myModal<?php echo $m ?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Update</h4>
                                      </div>
                                      <div class="modal-body">
                                    <form action="<?php echo base_url('SuperAdminController/updateShopAct/'.$all_shop_details[$m]['shop_id']) ?>"  method="POST">
                                        <input value="<?php echo $all_shop_details[$m]['shop_name'] ?>" name="name" type="text" class="form-control" placeholder="Name"><br>

                                        <input value="<?php echo $all_shop_details[$m]['shop_email'] ?>" name="email" type="text" class="form-control" placeholder="Email"><br>

                                        <input value="" name="password" type="text" class="form-control" placeholder="Password"><br>

                                        <input value="<?php echo $all_shop_details[$m]['shop_phone'] ?>" name="phone" type="text" class="form-control" placeholder="Phone"><br>

                                        <input value="<?php echo $all_shop_details[$m]['shop_lat'] ?>" name="lat" type="text" class="form-control" placeholder="Map lat"><br>

                                        <input value="<?php echo $all_shop_details[$m]['shop_lng'] ?>" name="lng" type="text" class="form-control" placeholder="Map lng"><br>
                                        <input type="submit" value="Add" class="btn btn-primary">
                                    </form>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>





                          </td>
                        </tr>
                    <?php } ?>
                  </tbody>
                </table>
            

        </div>
        
    </div>
</div>
</body>



</html>
<script src="<?php echo base_url("public/js/main.js") ?>"></script>

