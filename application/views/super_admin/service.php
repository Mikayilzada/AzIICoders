<?php

if(isset($_SESSION['super_admin_login']) == FALSE){ 
    redirect(base_url('SuperAdminController'));
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Super Admin Service</title>
    <link rel="stylesheet" href="<?php echo base_url('public/css/style.css') ?> ">
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>


<body>

<div class="header">
    <a href="#" id="menu-action">
        <i class="fa fa-bars"></i>
        <span>Close</span>
    </a>
    <div class="logo">
    </div>
</div>
<div class="sidebar">
    <ul>
        <li><a href="<?php echo base_url('SuperAdminController/admin') ?>"><i class="fa fa-home"></i><span>Main</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/problem') ?>"><i class="fa fa-exclamation-triangle"></i><span>Problem</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/shop') ?>"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span>Shop</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/services') ?>"><i class="fa fa-server"></i><span>Services</span></a></li>
        <li><a href="<?php echo base_url('SuperAdminController/logOut'); ?>"><i class="fa fa-sign-out"></i>Log out</a></li>
    </ul>
</div>

<!-- Content -->
<div class="main">
    <div class="hipsum">
    <div class="col-md-6">
        <br>
                <div class="jumbotron">
                    <h4>Add Service</h4>
                <?php echo $this->session->userdata('error_add_service'); $this->session->unset_userdata('error_add_service'); ?>
                <?php echo $this->session->userdata('success_add_service'); $this->session->unset_userdata('success_add_service'); ?>
                <form action="<?php echo base_url('SuperAdminController/addService'); ?>" method="POST">
                    
                        <div class="form-group">
                            <input type="text" name="service_name" placeholder="Service name" class="form-control">
                            <br>
                            <input type="number" name="service_phone" placeholder="Phone" class="form-control">
                            <br>
                            <input type="text" name="service_capacity" placeholder="Capacity" class="form-control">
                            <br>
                            <input type="text" name="service_email" placeholder="Email" class="form-control">
                            <br>
                            <input type="text" name="service_password" placeholder="Password" class="form-control">
                            <br>
                            <input type="submit" class="btn btn-primary" value="Add">
                        </div>
                </form>
            </div> 
    </div>
    <div class="col-md-6">
        <br>
        <div class="jumbotron">
            <h4>Add Service to shop</h4>
            
            <?php echo $this->session->userdata('success_add_shop_service'); $this->session->unset_userdata('success_add_shop_service'); ?>
            <?php echo $this->session->userdata('error_empty_shop_service'); $this->session->unset_userdata('error_empty_shop_service'); ?>
            <?php

              echo $this->session->userdata('error_add_shop_service_taken');
              $this->session->unset_userdata('error_add_shop_service_taken');

            ?>
            <form action="<?php echo base_url('SuperAdminController/addShopService'); ?>" method="POST">
                <select name="shop" id="" class="form-control">
                    <option value="">Choose Shop</option>
                    <?php foreach($all_shop as $all_shop_key){ ?>
                        <option value="<?php echo $all_shop_key['shop_id'] ?>"><?php echo $all_shop_key['shop_name'] ?></option>
                    <?php } ?>
                </select>
                <br>
                <select name="service" id="" class="form-control">
                    <option value="">Choose Service</option>
                    <?php foreach($all_services as $all_services_key){ ?>
                        <option value="<?php echo $all_services_key['service_id'] ?>"><?php echo $all_services_key['service_name'] ?></option>
                    <?php } ?>
                </select>
                <br>
                <input type="submit" value="Add" class="btn btn-primary">
            </form>
        </div>
    </div>    

        <div class="jumbotron">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Capacity</th>
                  <th scope="col">Operations</th>
                </tr>
              </thead>
              <tbody>

                <?php for($s=0;$s<count($all_services);$s++){ ?>
                    <tr>
                      <th scope="row"><?php echo $all_services[$s]['service_id'] ?></th>
                      <td><?php echo $all_services[$s]['service_name'] ?></td>
                      <td><?php echo $all_services[$s]['service_phone'] ?></td>
                      <td>
                        
                        <?php
                            $service_capacity_count = count($this->db->where('devices_status','1')->where('devices_service_id',$all_services[$s]['service_id'])->get('devices')->result_array());
                        ?> 
                        <?php echo $service_capacity_count.'/'.$all_services[$s]['service_capacity'] ?>
                       </td>

                        <td>
                          <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal<?php echo $s ?>">Update</button>
                          <a href="<?php echo base_url('SuperAdminController/deleteService/'.$all_services[$s]['service_id']) ?>" class="btn btn-danger btn-xs">Delete</a>





                        <div id="myModal<?php echo $s ?>" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Update</h4>
                              </div>
                              <div class="modal-body">
                                <form action="<?php echo base_url('SuperAdminController/updateService/'.$all_services[$s]['service_id']); ?>" method="POST">
                                  <input value="<?php echo $all_services[$s]['service_name'] ?>" type="text" class="form-control" name="name" placeholder="Service name"><br>
                                  <input value="<?php echo $all_services[$s]['service_phone'] ?>" type="text" class="form-control" name="phone" placeholder="Phone"><br>
                                  <input value="<?php echo $all_services[$s]['service_capacity'] ?>" type="text" class="form-control" name="capacity" placeholder="Capacity"><br>
                                  <input value="<?php echo $all_services[$s]['service_email'] ?>" type="text" class="form-control" name="email" placeholder="Email"><br>
                                  <input value="<?php echo $all_services[$s]['service_password'] ?>" type="text" class="form-control" name="password" placeholder="Password"><br>
                                  <input  type="submit" value="Change" class="btn btn-primary">
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>

                          </div>
                        </div>







                        </td>

                    </tr>                
                <?php } ?>

              </tbody>
            </table>
        </div>   

        <div class="jumbotron">
            <table class="table table-hover">
               <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Shop</th>
                  <th scope="col">Service</th>
                  <th scope="col">Operation</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($all_shop_and_services as $all_shop_and_services_key){ ?>
                    <tr>
                        <th><?php echo $all_shop_and_services_key['id'] ?></th>
                        <th><?php echo $all_shop_and_services_key['shop_name'] ?></th>
                        <th><?php echo $all_shop_and_services_key['service_name'] ?></th>
                        <th>
                          <a class="btn btn-danger btn-xs" href="<?php echo base_url('SuperAdminController/deleteServiceShop/'.$all_shop_and_services_key['id']) ?>">delete</a>
                        </th>
                    </tr>
                <?php } ?>

              </tbody>
            </table>
        </div>
    </div>
</div>
</body>



</html>
<script src="<?php echo base_url("public/js/main.js") ?>"></script>

