<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Super Admin Login</title>
    <link rel="stylesheet" href="<?php echo base_url("public/css/loginRegister.css") ?>">
    <script src="<?php echo base_url("public/js/loginRegister.js") ?>"></script>
</head>
<body>

<div class="row">
    <section class="section">
        <header>
            <h3>Super Admin Login</h3>
        </header>
        <main>
            <?php echo $this->session->userdata('error_super_admin_login'); $this->session->unset_userdata('error_super_admin_login'); ?>
            <?php echo $this->session->userdata('error_super_admin_empty'); $this->session->unset_userdata('error_super_admin_empty'); ?>
            <br><br>
            <form action="<?php echo base_url('SuperAdminController/loginAct') ?>" method="POST">
                <div class="form-item box-item">
                    <input type="text" name="email" placeholder="Email">
                </div>
                <div class="form-item box-item">
                    <input type="password" name="password" placeholder="Password">
                </div>
                <center>
                    <div class="form-item">
                        <input type="submit" id="submit" class="submit" value="Login">
                    </div>
                </center>
            </form>
        </main>
        <footer>
        </footer>
        <i class="wave"></i>
    </section>
</div>

</body>
</html>


