<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Service Login</title>
    <link rel="stylesheet" href="<?php echo base_url("public/css/loginRegister.css") ?>">
    <script src="<?php echo base_url("public/js/loginRegister.js") ?>"></script>
</head>
<body>

<div class="row">
    <section class="section">
        <header>
            <h3>Login</h3>
        </header>
        <main>
            <?php echo $this->session->userdata('service_login_error'); $this->session->unset_userdata('service_login_error'); ?>
            <?php echo $this->session->userdata('service_login_empty_error'); $this->session->unset_userdata('service_login_empty_error'); ?>
            <br><br>
            <form action="<?php echo base_url('ServiceController/loginAct'); ?>" method="POST">
                <div class="form-item box-item">
                    <input type="email" name="email" placeholder="Email">
                </div>
                <div class="form-item box-item">
                    <input type="password" name="password" placeholder="Password">
                </div>
                <center>
                    <div class="form-item">
                        <input type="submit" class="submit" id="submit" value="Login">
                    </div>
                </center>
            </form>
        </main>
        <footer>
        </footer>
        <i class="wave"></i>
    </section>
</div>

</body>
</html>


