<?php

if(isset($_SESSION['service_login']) == FALSE){
    redirect(base_url('ServiceController'));
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Service All Device</title>
    <link rel="stylesheet" href="<?php echo base_url('public/css/style.css') ?> ">
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>

<div class="header">
    <a href="#" id="menu-action">
        <i class="fa fa-bars"></i>
        <span>Close</span>
    </a>
    <div class="logo">
        Service Admin
    </div>
</div>
<div class="sidebar">
    <ul>
        <li><a href="<?php echo base_url('ServiceController/admin') ?>"><i class="fa fa-desktop"></i><span>Main</span></a></li>
        <li><a href="<?php echo base_url('ServiceController/device') ?>"><i class="fa fa-server"></i><span>Devices</span></a></li>
        <li><a href="#"><i class="fa fa-users"></i><span>Staff</span></a></li>
        <li><a href="#"><i class="fa fa-envelope-o"></i><span>Messages</span></a></li>
        <li><a href="<?php echo base_url('ServiceController/logOut') ?>"><i class="fa fa-sign-out"></i><span>Log out</span></a></li>
    </ul>
</div>

<!-- Content -->
<div class="main">
    <div class="hipsum">
        <div class="jumbotron">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Name</th>
                  <!-- <th scope="col">Completeleness</th> -->
                  <th scope="col">Status</th>
                  <th scope="col">Description</th>
                  <th scope="col">Repair time</th>
                  <th scope="col">Repair day</th>
                  <th scope="col">Price</th>
                  <th scope="col">Add Device Info</th>
                </tr>
              </thead>
              <tbody>


                <?php for($s=0;$s<count($all_service_devices);$s++){ ?>
                    <tr>
                      <th><?php echo $s+1 ?></th>
                      <td><?php echo $all_service_devices[$s]['devices_name'] ?></td>
                      <td>
                        <?php

                        $start_date = strtotime($all_service_devices[$s]['devices_start_date']);
                        $end_date = strtotime($all_service_devices[$s]['devices_end_date']);
                        $now = strtotime(date("Y-m-d H:i:s"));

                        $interval_start_end = $end_date - $start_date;
                        $interval_start_now = $now - $start_date;

                        if($start_date < $now){
                             echo round(($interval_start_now/$interval_start_end)*100,2).'%';
                        }else{
                             echo "Not started";
                        }
 

                        ?>

                         </td>
                      <td>
                        <?php
                            if($all_service_devices[$s]['devices_status'] == 0){
                                echo "<span style='color:orange'>Pending</span>";
                            }elseif($all_service_devices[$s]['devices_status'] == 1){
                                echo "<span style='color:blue'>Start Repair</span>";
                            }elseif($all_service_devices[$s]['devices_status'] == 2){
                                echo "<span style='color:green'>Finished</span>";
                            }elseif($all_service_devices[$s]['devices_status'] == 3){
                                echo "<span style='color:red'>Aborted</span>";
                            }
                        ?>  
                      </td>
                      <td><?php echo $all_service_devices[$s]['devices_desc'] ?></td>

                      <td>
                        

                        <?php

                        if(!empty($all_service_devices[$s]['devices_start_date'])){
                           echo strftime('%d/%m/%y %H:%M', strtotime($all_service_devices[$s]['devices_start_date'])).' - '.strftime('%d/%m/%y %H:%M', strtotime($all_service_devices[$s]['devices_end_date']));
                        }else{
                            echo "-";
                        }


                         ?>
                          


                      </td>
                      <td>

    
    <?php $st = strftime('%d/%m/%y %H:%M', strtotime($all_service_devices[$s]['devices_start_date'])); ?>
    <?php $en = strftime('%d/%m/%y %H:%M', strtotime($all_service_devices[$s]['devices_end_date'])); ?>

    <?php echo $en - $st ?>

                      </td>
                      <td><?php echo $all_service_devices[$s]['devices_price'] ?> AZN</td>
                      <td>
                          <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal<?php echo $s ?>">Add Repair time</button>
                            
                          <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myDeviceModal<?php echo $s ?>">Add Device status</button>

                      </td>
                    </tr>

                    <div id="myModal<?php echo $s ?>" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Repair Time</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                                <form action="<?php echo base_url('ServiceController/addStartAndEndDate/'.$all_service_devices[$s]['devices_id']); ?>" method="POST">
                                    <div class="col-md-6">
                                        <label for="">Start date</label>
                                        <input name="start_date" type="datetime-local" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">End date</label>
                                        <input name="end_date" type="datetime-local" class="form-control">
                                    </div>
                                    <br><br><br><br>
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-primary" value="Add Date">
                                    </div>
                                </form>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>


                    <div id="myDeviceModal<?php echo $s ?>" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Device Status</h4>
                          </div>
                          <div class="modal-body">
                            <form action="<?php echo base_url('ServiceController/addDeviceStatus/'.$all_service_devices[$s]['devices_id']); ?>" method="POST">
                                <select name="device_status" class="form-control">
                                    <option <?php if($all_service_devices[$s]['devices_status'] == 0){ echo "SELECTED";}; ?> value="0">Pending</option>
                                    <option <?php if($all_service_devices[$s]['devices_status'] == 1){ echo "SELECTED";}; ?> value="1">Start Repair</option>
                                    <option <?php if($all_service_devices[$s]['devices_status'] == 2){ echo "SELECTED";}; ?> value="2">Finished</option>
                                    <option <?php if($all_service_devices[$s]['devices_status'] == 3){ echo "SELECTED";}; ?> value="3">Aborted</option>
                                </select>
                                <br>
                                <input type="submit" value="Add status" class="btn btn-primary">
                            </form>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>



                <?php } ?>


              </tbody>
            </table>
        </div>
    </div>
</div>
</body>



</html>
<script src="<?php echo base_url("public/js/main.js") ?>"></script>

