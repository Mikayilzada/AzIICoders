<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url('public/css/main.css') ?> ">

</head>
<body>
<!-- navigation panel -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-main">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-collapse-main">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#home">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#information">Information</a></li>
                <li><a href="#google_map">Contact</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<!-- first section - Home -->
<div id="home" class="home">
    <div class="text-vcenter">
        <h1>Azercell</h1>
        <h3>Welcome to the repair service of the devices.</h3>
<!--        <a href="#about" class="btn btn-default btn-lg">Continue</a>-->
    </div>
</div>
<!-- /first section -->

<!-- second section - About -->
<div id="about" class="pad-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="http://modern.az/uploads/news/azercell-logo1495547922.png" alt="" />
            </div>
            <div class="col-sm-6 text-center">
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in sem cras amet.</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum metus et ligula venenatis, at rhoncus nisi molestie. Pellentesque porttitor elit suscipit massa laoreet metus.</p>
            </div>
        </div>
    </div>
</div>
<!-- /second section -->

<!-- third section - Services -->
<div id="services" class="pad-section">
    <div class="container">
        <h2 class="text-center">Our Services</h2> <hr />
        <div class="row text-center">
            <div class="col-sm-3 col-xs-6">
<!--                <i class="glyphicon glyphicon-cloud "> </i>-->
                <i class="fa fa-4x fa-desktop" aria-hidden="true"></i>
                <h4>PC</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in sem cras amet. Donec in sem cras amet.</p>
            </div>
            <div class="col-sm-3 col-xs-6">
                <i class="fa fa-4x fa-mobile" aria-hidden="true"></i>
                <h4>Mobile</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in sem cras amet. Donec in sem cras amet.</p>
            </div>
            <div class="col-sm-3 col-xs-6">
                <i class="fa fa-4x fa-laptop" aria-hidden="true"></i>
                <h4>Laptop</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in sem cras amet. Donec in sem cras amet.</p>
            </div>
            <div class="col-sm-3 col-xs-6">
                <i class="fa fa-4x fa-pie-chart" aria-hidden="true"></i>
                <h4>Data card</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in sem cras amet. Donec in sem cras amet.</p>
            </div>
        </div>
    </div>
</div>
<!-- /third section -->

<!-- fourth section - Information -->
<div id="information" class="pad-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Additional information</h2>
                    </div>
                    <div class="panel-body lead">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit adipiscing blandit. Aliquam placerat, velit a fermentum fermentum, mi felis vehicula justo, a dapibus quam augue non massa. Duis euismod, augue et tempus consequat, lorem mauris porttitor quam, consequat ultricies mauris mi a metus. Phasellus congue, leo sed ultricies tristique, nunc libero tempor ligula, at varius mi nibh in nisi. Aliquam erat volutpat. Maecenas rhoncus, neque facilisis rhoncus tempus, elit ligula varius dui, quis amet.
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="panel-title">Additional information</h2>
                    </div>
                    <div class="panel-body lead">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed hendrerit adipiscing blandit. Aliquam placerat, velit a fermentum fermentum, mi felis vehicula justo, a dapibus quam augue non massa. Duis euismod, augue et tempus consequat, lorem mauris porttitor quam, consequat ultricies mauris mi a metus. Phasellus congue, leo sed ultricies tristique, nunc libero tempor ligula, at varius mi nibh in nisi. Aliquam erat volutpat. Maecenas rhoncus, neque facilisis rhoncus tempus, elit ligula varius dui, quis amet.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /fourth section -->

<!-- fifth section -->
<div id="services" class="pad-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h3>So far, 187 units have been repaired.</h3>
<!--                <h4>The next is the address on Google maps</h4>-->
            </div>
        </div>
    </div>
</div>
<!-- /fifth section -->

<!-- google map -->
<!--<div id="google_map"></div>-->
<!-- /google map -->

<!-- footer -->
<footer>
    <hr />
    <div class="container">
        <p class="text-right">Copyright &copy; AziiCoders 2018</p>
    </div>
</footer>
<!-- /footer -->


</body>
</html>
<script src="<?php echo base_url("public/js/mainJs.js") ?>"></script>

