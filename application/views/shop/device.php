<?php

if(isset($_SESSION['shop_login']) == FALSE){
    redirect(base_url('ShopController'));
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shop All Device</title>
    <link rel="stylesheet" href="<?php echo base_url('public/css/style.css') ?> ">
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>

<div class="header">
    <a href="#" id="menu-action">
        <i class="fa fa-bars"></i>
        <span>Close</span>
    </a>
    <div class="logo">
        <?php echo $_SESSION['shop_data'][0]['shop_name'] ?>
    </div>
</div>
<div class="sidebar">
    <ul>
        <li><a href="<?php echo base_url('ShopController/admin') ?>"><i class="fa fa-home"></i><span>Main</span></a></li>
        <li><a href="#"><i class="fa fa-users"></i><span>Users</span></a></li>
        <li><a href="<?php echo base_url('ShopController/device') ?>"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>Devices</span></a></li>
        <li><a href="<?php echo base_url('ShopController/services') ?>"><i class="fa fa-server"></i><span>Services</span></a></li>
        <li><a href="<?php echo base_url('ShopController/logOut'); ?>"><i class="fa fa-sign-out"></i>Log out</a></li>
    </ul>
</div>

<!-- Content -->
<div class="main">
    <div class="hipsum">
        <div class="jumbotron">


<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Name</th>
      <th scope="col">Completeleness</th>
      <th scope="col">Service</th>
      <th scope="col">Status</th>
      <th scope="col">Description</th>
      <th scope="col">Repair time</th>
      <th scope="col">Price</th>
      <th scope="col">More</th>
    </tr>
  </thead>
  <tbody>


    <?php for($s=0;$s<count($all_shop_devices);$s++){ ?>
        <tr>
          <th scope="row"><?php echo $all_shop_devices[$s]['devices_id'] ?></th>
          <td><?php echo $all_shop_devices[$s]['devices_name'] ?></td>
          <td><?php echo $all_shop_devices[$s]['devices_completeleness'] ?> %</td>
          <td>

            <?php

                if($all_shop_devices[$s]['devices_service_id'] == 0){
                    echo 'No service';
                }else{
                    echo $all_shop_devices[$s]['service_name'];
                }

            ?>
                
          </td>
          <td>
                
            <?php

            if($all_shop_devices[$s]['devices_status'] == 0){
                echo "Pending";
            }elseif($all_shop_devices[$s]['devices_status'] == 1){

                echo "Repair started";
            }elseif($all_shop_devices[$s]['devices_status'] == 2){
                echo "Finished";
            }elseif($all_shop_devices[$s]['devices_status'] == 3){
                echo "Aborted";
            }
            ?>


          </td>
          <td><?php echo $all_shop_devices[$s]['devices_desc'] ?></td>
          <td><?php echo $all_shop_devices[$s]['devices_start_date'].'<br>'.$all_shop_devices[$s]['devices_end_date'] ?></td>
          <td><?php echo $all_shop_devices[$s]['devices_price'] ?> AZN</td>
          <td>
              <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myUserModal<?php echo $s ?>">User info</button>

                <?php

                  if($all_shop_devices[$s]['devices_service_id'] != 0){
                    echo "<b style='color:green'>Service selected</b>";
                  }else{ ?>
<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal<?php echo $s ?>">Choose Service</button>
                  <?php }

                ?>
                


          </td>
        </tr>



          <div class="modal fade" id="myUserModal<?php echo $s ?>" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">User Info</h4>
                </div>
                <div class="modal-body">
                    <span><b>Name</b>: <?php echo $all_shop_devices[$s]['name'] ?></span>
                    <br>

                    <span><b>Phone</b>: <?php echo $all_shop_devices[$s]['number'] ?></span>
                    <br>

                    <span><b>Gender</b>: <?php echo $all_shop_devices[$s]['gender'] ?></span>
                    <br>

                    <span><b>Email</b>: <?php echo $all_shop_devices[$s]['email'] ?></span>
                    <br>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
              </div>
              
            </div>
          </div>





          <div class="modal fade" id="myModal<?php echo $s ?>" role="dialog">
            <div class="modal-dialog">
            
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"><?php echo $all_shop_devices[$s]['devices_name'] ?></h4>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('ShopController/addDeviceService/'.$all_shop_devices[$s]['devices_id'].'/'.$all_shop_devices[$s]['devices_user_id'] )?>" method="POST">
                        

                    <label for="">Choose repair service</label>
                    <select name="shop" id="" class="form-control">
                        <option value="">Choose service</option>
                        <?php foreach($all_shop_devices_data as $all_shop_devices_data_key){ ?>
                            
                            <?php $data_service_copacity = count($this->db->where('devices_service_id',$all_shop_devices_data_key['service_id'])->get('devices')->result_array());
                              
                              if($data_service_copacity >= $all_shop_devices_data_key['service_capacity']){
                                
                              }else{ ?>
<option value="<?php echo $all_shop_devices_data_key['service_id'] ?>"><?php echo $all_shop_devices_data_key['service_name'] ?></option>
                              <?php }
                             ?>

                            
                        <?php } ?>
                        
                    </select>
                    <br>
                    <input type="submit" class="btn btn-primary" value="Add Service">


                    </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
              </div>
              
            </div>
          </div>





    <?php } ?>


  </tbody>
</table>

        </div>
        
    </div>
</div>
</body>



</html>
<script src="<?php echo base_url("public/js/main.js") ?>"></script>

