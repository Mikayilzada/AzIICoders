<?php

if(isset($_SESSION['shop_login']) == FALSE){
    redirect(base_url('ShopController'));
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Shop Home</title>
    <link rel="stylesheet" href="<?php echo base_url('public/css/style.css') ?> ">
<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>


<body>

<div class="header">
    <a href="#" id="menu-action">
        <i class="fa fa-bars"></i>
        <span>Close</span>
    </a>
    <div class="logo">
       <?php echo $_SESSION['shop_data'][0]['shop_name'] ?>
    </div>
</div>
<div class="sidebar">
    <ul>
        <li><a href="<?php echo base_url('ShopController/admin') ?>"><i class="fa fa-home"></i><span>Main</span></a></li>
        <li><a href="#"><i class="fa fa-users"></i><span>Users</span></a></li>
        <li><a href="<?php echo base_url('ShopController/device') ?>"><i class="fa fa-bar-chart" aria-hidden="true"></i><span>Devices</span></a></li>
        <li><a href="<?php echo base_url('ShopController/services') ?>"><i class="fa fa-server"></i><span>Services</span></a></li>
        <li><a href="<?php echo base_url('ShopController/logOut'); ?>"><i class="fa fa-sign-out"></i>Log out</a></li>
    </ul>
</div>

<!-- Content -->
<div class="main">
    <div class="hipsum">
        <div class="jumbotron">
asdads
        </div>
        
    </div>
</div>
</body>



</html>
<script src="<?php echo base_url("public/js/main.js") ?>"></script>

